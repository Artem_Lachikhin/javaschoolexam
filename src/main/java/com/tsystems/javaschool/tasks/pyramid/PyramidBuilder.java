package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.sqrt;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {


        if(inputNumbers.size()>Integer.MAX_VALUE/2)
            throw new CannotBuildPyramidException();

        try {
            Collections.sort(inputNumbers);


            double floatRows = (1.0 + sqrt(1.0 + 8.0 * inputNumbers.size())) / 2.0;

            int rows = (int) floatRows;

            if (Math.abs(floatRows - (double) rows) > 1.0e-10)
                throw new CannotBuildPyramidException();

            rows--;

            int cols = 2 * rows - 1;

            int[][] result = new int[rows][cols];

            for (int[] col : result) {
                Arrays.fill(col, 0);
            }
            int xBasic = (cols + 1) / 2;
            int prevY = -1;
            int x = 0;


            for (int i = 0; i < inputNumbers.size(); i++) {
                double dy = (1.0 + sqrt(1.0 + 8.0 * (i + 1))) / 2.0;
                int y = (int) Math.ceil(dy) - 2;
                if (y != prevY) {
                    xBasic--;
                    x = xBasic;
                    prevY = y;
                } else {
                    x = x + 2;
                }
                result[y][x] = inputNumbers.get(i);
            }
            return result;
        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }

    }


}
