package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.*;
import java.text.DecimalFormatSymbols;
public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private static DecimalFormat _decimalFormat = new DecimalFormat("0.####",new DecimalFormatSymbols(Locale.ROOT));


    public String evaluate(String statement) {
        try {
            return calc(parse(statement));
        }
        catch ( Exception e)
        {
            return null;
        }


    }


    private int index(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    private boolean isSymbol(String token) {
        String SymbolsBrackets = "() +-*/";
        if (token.length() != 1) return false;
        for (int i = 0; i < SymbolsBrackets.length(); i++) {
            if (token.charAt(0) == SymbolsBrackets.charAt(i)) return true;
        }
        return false;
    }

    private boolean isOperator(String token) {
        String Symbols = "+-*/";
        if (token.equals("u-")) return true;
        for (int i = 0; i < Symbols.length(); i++) {
            if (token.charAt(0) == Symbols.charAt(i)) return true;
        }
        return false;
    }


    public List<String> parse(String infix) {
        List<String> polish = new ArrayList<String>();
        Deque<String> stack = new ArrayDeque<String>();
        StringTokenizer tokenizer = new StringTokenizer(infix, "() +-*/", true);
        String prev = "";
        String curr = "";
        while (tokenizer.hasMoreTokens()) {
            curr = tokenizer.nextToken();
            if (!tokenizer.hasMoreTokens() && isOperator(curr))
                return null;
            if (curr.equals(" ")) continue;
            else if (isSymbol(curr)) {
                if (curr.equals("(")) stack.push(curr);
                else if (curr.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        polish.add(stack.pop());
                        if (stack.isEmpty())
                            break;
                    }
                    stack.pop();
                } else {
                    if (curr.equals("-") && !prev.equals("-") && (prev.equals("") || (isSymbol(prev) && !prev.equals(")")))) {
                        // унарный минус
                        curr = "u-";
                    } else {
                        while (!stack.isEmpty() && (index(curr) <= index(stack.peek()))) {
                            polish.add(stack.pop());
                        }
                    }
                    stack.push(curr);
                }
            } else {
                polish.add(curr);
            }
            prev = curr;
        }
        while (!stack.isEmpty()) {
            if (isSymbol(stack.peek())) polish.add(stack.pop());
            else {
                return null;
            }
        }
        return polish;
    }

    public String calc(List<String> polish) {
        Deque<Double> stack = new ArrayDeque<>();
        for (String x : polish) {
            if (x.equals("+")) stack.push(stack.pop() + stack.pop());
            else if (x.equals("-")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a - b);
            } else if (x.equals("*")) stack.push(stack.pop() * stack.pop());
            else if (x.equals("/"))
            {
                Double b = stack.pop(), a = stack.pop();
                Double c = a / b ;
                if( c == Double.NEGATIVE_INFINITY || c == Double.POSITIVE_INFINITY)
                    throw new IllegalArgumentException();
                stack.push(c);

            }
            else if (x.equals("u-")) stack.push(-stack.pop());   /////???????????????
            else stack.push(Double.valueOf(x));
        }
        return  _decimalFormat.format(Double.valueOf(stack.pop()));
    }
}


